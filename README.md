# resubmit

#### 介绍
就是表单重复提交的处理方式之一:本地锁

#### 软件架构
软件架构说明:
就用到了springboot2.1.4来实现的,主要用的是aop
##### 原理
使用了 ConcurrentHashMap 并发容器 putIfAbsent 方法,
和 ScheduledThreadPoolExecutor 定时任务 就是将提交的内容
参数和参数名称统一加密MD5作为key存到ConcurrentHashMap中,规定时间内如果重复提交相同的内容
这个key也会一样,在put到map的时候如果存在则直接返回已经提交了.
MD5在一定范围类认为是唯一的 近似唯一  当然在低并发的情况下足够了 
当然本地锁只适用于单机部署的应用.
###### 方法2:
也可以使用guava cache机制, gauva中有配有缓存的有效时间 也是可以的key的生成 



#### 设计思路

1.  配置注解 com.example.resubmit.annotation.Resubmit
2.  实现一个锁 com.example.resubmit.util.ResubmitLock
3.  定义一个AOP切面, 专门切自定义的注解@Resubmit(delaySeconds = 10)
这个切面里实现拦截逻辑,如果重复的内容,加入到map中发现已经存在那就提示"已提交",并使用定时器20秒后删除这个key com.example.resubmit.aspect.ResubmitDataAspect
4.   

#### 使用说明

就直接在controller上面增加这个注解
```    @GetMapping(value = "/save")
       @Resubmit(delaySeconds = 10)
       public Object saveSomethings(String name) {
           String ss = name + "已经保存";
           System.out.println(ss);
           return ss;
       }
```

更加具体可以参考[幂等问题 8种方案解决重复提交](https://juejin.im/post/5d31928c51882564c966a71c)
