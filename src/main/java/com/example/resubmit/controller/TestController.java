package com.example.resubmit.controller;

import com.example.resubmit.annotation.Resubmit;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {


    @GetMapping(value = "/save")
    @Resubmit(delaySeconds = 10)
    public Object saveSomethings(String name) {
        String ss = name + "已经保存";
        System.out.println(ss);
        return ss;
    }
}
