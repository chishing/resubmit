package com.example.resubmit.annotation;

import java.lang.annotation.*;

/**
 * 自定义注解,关于限制重复提交 20秒时间间隔
 * @author james
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Resubmit {
    /**
     * 延迟时间
     * @return
     */
    int delaySeconds() default 20;
}
